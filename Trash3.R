##Worldclim
setwd("~/Desktop/R_Hole")
library("raster", lib.loc="/Library/Frameworks/R.framework/Versions/3.2/Resources/library")
library("rgdal", lib.loc="/Library/Frameworks/R.framework/Versions/3.2/Resources/library")

preclist <- list.files("~/Desktop/R_Hole/worldclim/prec/", pattern = ".bil")
wd <-getwd()
setwd("~/Desktop/R_Hole/worldclim/prec/")
prec <- stack(preclist)
setwd(wd)
prec1=raster("~/Desktop/R_Hole/worldclim/prec/prec1.bil")
prec1
plot(prec1)

alt=raster("~/Desktop/R_Hole/worldclim/alt/alt.bil")
plot(alt)

alt

prec2=raster("~/Desktop/R_Hole/worldclim/prec/prec2.bil")
plot(prec2)

picabi.rast=raster("~/Desktop/R_Hole/raster.picabi.intersect/picabi.intersect.grd")
plot(picabi.rast)

realt=resample(alt,picabi.rast)
plot(realt)
picabialt=merge(picabi.rast,realt)
plot(picabialt)

reprec=resample(prec1,picabi.rast)
plot(reprec)

picabi.int=readOGR("picabi.int","picabi.int")

##Subalpine alt and precipitation
hhcell=extract(prec, picabi.int, fun=NULL, na.rm=FALSE, weights=FALSE,normalizeWeights=TRUE, cellnumbers=TRUE, small=TRUE, df=TRUE,factors=FALSE, sp=FALSE)
hhcell.xy <- density(hhcell$prec1)
plot(hhcell.xy, xlim=c(0,200))

aaa=extract(alt, picabi.int, fun=NULL, na.rm=FALSE, weights=FALSE,normalizeWeights=TRUE, cellnumbers=T, small=TRUE, df=TRUE,factors=FALSE, sp=FALSE)
aaa.xy <- density(aaa$alt)
plot(aaa.xy)


##northwest altitude and rainfall
pictsu.int=readOGR("pictsu.int","pictsu.int")
pictsu.alt=extract(alt, pictsu.int, cellnumbers=TRUE, df=TRUE)
head(pictsu.alt)
pictsu.alt.xy=density(pictsu.alt$alt, na.rm=TRUE)

pictsu.prec=extract(prec1, pictsu.int, cellnumbers=TRUE, df=TRUE)
pictsu.prec.xy=density(pictsu.prec$prec1)

##comparing picabi alt and pictsu.alt
aaa.xy <- density(aaa$alt)
pictsu.alt.xy=density(pictsu.alt$alt,na.rm=TRUE)
plot(aaa.xy)
lines(pictsu.alt.xy)

##comparing picabi prec hhcell with pictsu.prec
hhcell.xy <- density(hhcell$prec1)
pictsu.prec.xy=density(pictsu.prec$prec1,na.rm=T)
plot(pictsu.prec.xy,ylim=c(0,.030))
lines(hhcell.xy)




